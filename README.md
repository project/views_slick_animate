Views slick animate

Features
1. slick slider without animate available
2. slick slider with animate available
3. slick slider with random animate available
4. slick slider image with zoom in and zoom out animation available
5. Fully accessible with arrow key navigation.
6. Built-in lazyLoad, and multiple breakpoint options.
7. Random, autoplay, pagers, etc...
8. Supports text, responsive image/ picture
9. Works with Views use Built
10. response device settings tab available
11. animate settings tab available
12. animate random settings tab available

How to use in module 

install enable module

go to view create then set the view mode seting view slick animation